<?php
/**
 * @category Scandi
 * @package  Scandi_EmailTemplate
 */
class Scandi_EmailTemplate_Model_Core_Email_Template extends Mage_Core_Model_Email_Template
{
    /**
     * Load default email template from locale translate
     *
     * @param string $templateId
     * @param string $locale
     * @return Scandi_EmailTemplate_Model_Core_Email_Template
     */
    public function loadDefault($templateId, $locale=null)
    {
        $storeConfig = array(Mage_Core_Model_Translate::CONFIG_KEY_STORE => $this->getDesignConfig()->getStore());
        Mage::app()->getTranslator()->setConfig($storeConfig);
        return parent::loadDefault($templateId, $locale);
    }
}