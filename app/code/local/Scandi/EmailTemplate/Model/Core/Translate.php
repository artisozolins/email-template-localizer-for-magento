<?php
/**
 * @category Scandi
 * @package  Scandi_EmailTemplate
 */
class Scandi_EmailTemplate_Model_Core_Translate extends Mage_Core_Model_Translate
{
    /**
     * Retrieve translated template file, check in themes folder first
     *
     * @param string $file
     * @param string $type
     * @param string $localeCode
     * @return string
     */
    public function getTemplateFile($file, $type, $localeCode=null)
    {
        if (is_null($localeCode) || preg_match('/[^a-zA-Z_]/', $localeCode)) {
            $localeCode = $this->getLocale();
        }

        $store = $this->getConfig(self::CONFIG_KEY_STORE);
        $initialInfo = Mage::getSingleton('core/app_emulation')->startEnvironmentEmulation($store);
        $filePath = Mage::getDesign()->getLocaleFileName('template' . DS . $type . DS . $file);
        Mage::getModel('core/app_emulation')->stopEnvironmentEmulation($initialInfo);

        if (!file_exists($filePath)) {
            $filePath = Mage::getBaseDir('locale')  . DS
                . $localeCode . DS . 'template' . DS . $type . DS . $file;
        }

        if (!file_exists($filePath)) { // If no template specified for this locale, use store default
            $filePath = Mage::getBaseDir('locale') . DS
                . Mage::app()->getLocale()->getDefaultLocale()
                . DS . 'template' . DS . $type . DS . $file;
        }

        if (!file_exists($filePath)) {  // If no template specified as  store default locale, use en_US
            $filePath = Mage::getBaseDir('locale') . DS
                . Mage_Core_Model_Locale::DEFAULT_LOCALE
                . DS . 'template' . DS . $type . DS . $file;
        }

        $ioAdapter = new Varien_Io_File();
        $ioAdapter->open(array('path' => Mage::getBaseDir('locale')));

        return (string) $ioAdapter->read($filePath);
    }
}